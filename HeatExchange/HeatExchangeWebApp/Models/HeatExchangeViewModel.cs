﻿using HeatExchangeWebApp.Data;

namespace HeatExchangeWebApp.Models
{
    public class HeatExchangeViewModel
    {
        public string Message { get; set; }
        public HeatExchangeInputViewModel Input;
        public HeatExchangeOutputViewModel Output;

        public List<History> History;
        public bool IsAuth { get; set; }
        public string Login { get; set; }
    }

    public class HeatExchangeInputViewModel
    {
        // Начальные значения
        public double H0 { get; set; } // Высота слоя, м
        public double tm0 { get; set; } // Начальная температура материала, градусы
        public double Tg0 { get; set; } // Начальная температура газа, градусы
        public double Wg { get; set; } // Скорость газа на свободное сечение шахты, м/с
        public double Cg { get; set; } // Средняя теплоемкость газа, кДж/(м^3 * К)
        public double Cm { get; set; } // Средняя теплоемкость материала, кДж/(м^3 * К)
        public double Gm { get; set; } // Расход материалов, кг/с
        public double D { get; set; } // Диаметр аппарата, м
        public double Av { get; set; } // Объемный коэффициент теплоотдачи, Вт/(м^3 * К)
    }

    public class HeatExchangeOutputViewModel
    {
        // Расчетные значения
        public double m;
        public double Y0;
        public double IntermediateValue;

        //Расчетные табличные значения
        public double[] Y;
        public double[] IntermediateValue_1;
        public double[] IntermediateValue_2;
        public double[] ϑ;
        public double[] θ;
        public double[] tm;
        public double[] Tg;
        public double[] DifferenceTemperatures;
    }
}