﻿using System.ComponentModel.DataAnnotations;

namespace HeatExchangeWebApp.Data
{
    public class History
    {
        [Key]
        public int Id { get; set; }
        public int? UserId { get; set; }
        public double H0 { get; set; }
        public double tm0 { get; set; }
        public double Tg0 { get; set; }
        public double Wg { get; set; }
        public double Cg { get; set; }
        public double Cm { get; set; }
        public double Gm { get; set; }
        public double D { get; set; }
        public double Av { get; set; }

    }
}
