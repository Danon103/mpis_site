﻿using System.ComponentModel.DataAnnotations;

namespace HeatExchangeWebApp.Data
{
    public class Users
    {
        [Key]
        public int Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }

    }
}
