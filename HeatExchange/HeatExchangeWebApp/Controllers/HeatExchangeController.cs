﻿using Microsoft.AspNetCore.Mvc;
using HeatExchangeWebApp.Models;
using HeatExchangeLib;
using HeatExchangeWebApp.Data;

namespace HeatExchangeWebApp.Controllers
{
    public class HeatExchangeController : Controller
    {
        private readonly ApplicationDbContext _dbСontext;
        public HeatExchangeController(ApplicationDbContext context)
        {
            _dbСontext = context;
        }

        // GET ----------------------------------------------------------------------------------
        [HttpGet]
        public IActionResult Index(int? historyId)
        {
            var HEVM = new HeatExchangeViewModel();
            HEVM.Input = new HeatExchangeInputViewModel
            {
                H0 = 6,
                tm0 = 750,
                Tg0 = 30,
                Wg = 0.5,
                Cg = 1.32,
                Cm = 1.49,
                Gm = 1.72,
                D = 2.3,
                Av = 2460
            };
            HEVM.Output = new HeatExchangeOutputViewModel
            {
                m = 0.93,
                Y0 = 22.36,
                Y = new double[] { 0.00, 1.86, 3.73, 5.59, 7.45, 9.32, 11.18, 13.05, 14.91, 16.77, 18.64, 20.50, 22.36 },
                IntermediateValue_1 = new double[] { 0.00, 0.12, 0.23, 0.32, 0.40, 0.48, 0.54, 0.60, 0.64, 0.69, 0.73, 0.76, 0.79 },
                IntermediateValue_2 = new double[] { 0.06, 0.18, 0.28, 0.37, 0.44, 0.51, 0.57, 0.62, 0.67, 0.71, 0.74, 0.77, 0.80 },
                ϑ = new double[] { 0.00, 0.15, 0.28, 0.40, 0.50, 0.59, 0.67, 0.74, 0.80, 0.86, 0.90, 0.95, 0.98 },
                θ = new double[] { 0.08, 0.22, 0.35, 0.46, 0.55, 0.64, 0.71, 0.78, 0.83, 0.88, 0.93, 0.97, 1.00 },
                tm = new double[] { 750, 641, 545, 461, 387, 322, 265, 215, 171, 132, 98, 69, 42 },
                Tg = new double[] { 692, 590, 500, 422, 353, 292, 239, 192, 150, 114, 82, 55, 30 },
                DifferenceTemperatures = new double[] { 58, 51, 45, 40, 35, 31, 27, 24, 21, 18, 16, 14, 12 }
            };

            if (historyId != null)
            {
                var historyById = _dbСontext.History.FirstOrDefault(X => X.Id == historyId);
                var history = _dbСontext.History.Where(X => X.Id == historyById.Id).ToList();
                HEVM.Input.H0 = history[0].H0;
                HEVM.Input.tm0 = history[0].tm0;
                HEVM.Input.Tg0 = history[0].Tg0;
                HEVM.Input.Wg = history[0].Wg;
                HEVM.Input.Cg = history[0].Cg;
                HEVM.Input.Cm = history[0].Cm;
                HEVM.Input.Gm = history[0].Gm;
                HEVM.Input.D = history[0].D;
                HEVM.Input.Av = history[0].Av;
            }

            var login = HttpContext.Session.GetString("login");
            if(!string.IsNullOrWhiteSpace(login))
            {
                HEVM.IsAuth = true;
                HEVM.Login = login;
                var user = _dbСontext.Users.FirstOrDefault(X => X.Login == login);

                if(user != null)
                    HEVM.History = _dbСontext.History.Where(X => X.UserId == user.Id).ToList();
            }
                
            return View(HEVM);
        }

        [HttpGet]
        public IActionResult Delete(int? historyId)
        {
            if (historyId != null)
            {
                var historyById = _dbСontext.History.FirstOrDefault(X => X.Id == historyId);
                _dbСontext.History.Remove(historyById);
                _dbСontext.SaveChanges();
            }

            return RedirectToAction(nameof(Index));
        }

        [HttpGet]
        public IActionResult DeleteAll()
        {
            var login = HttpContext.Session.GetString("login");
            if (!string.IsNullOrWhiteSpace(login))
            {
                var user = _dbСontext.Users.FirstOrDefault(X => X.Login == login);
                if (user != null)
                {
                    var history = _dbСontext.History.Where(X => X.UserId == user.Id).ToList();
                    foreach (var elem in history)
                    {
                        _dbСontext.History.Remove(elem);
                    }
                    _dbСontext.SaveChanges();
                }
            }

            return RedirectToAction(nameof(Index));
        }

        [HttpGet]
        public IActionResult Exit()
        {
            var HEVM = new HeatExchangeViewModel();
            HEVM.Input = new HeatExchangeInputViewModel
            {
                H0 = 6,
                tm0 = 750,
                Tg0 = 30,
                Wg = 0.5,
                Cg = 1.32,
                Cm = 1.49,
                Gm = 1.72,
                D = 2.3,
                Av = 2460
            };
            HEVM.Output = new HeatExchangeOutputViewModel
            {
                m = 0.93,
                Y0 = 22.36,
                Y = new double[] { 0.00, 1.86, 3.73, 5.59, 7.45, 9.32, 11.18, 13.05, 14.91, 16.77, 18.64, 20.50, 22.36 },
                IntermediateValue_1 = new double[] { 0.00, 0.12, 0.23, 0.32, 0.40, 0.48, 0.54, 0.60, 0.64, 0.69, 0.73, 0.76, 0.79 },
                IntermediateValue_2 = new double[] { 0.06, 0.18, 0.28, 0.37, 0.44, 0.51, 0.57, 0.62, 0.67, 0.71, 0.74, 0.77, 0.80 },
                ϑ = new double[] { 0.00, 0.15, 0.28, 0.40, 0.50, 0.59, 0.67, 0.74, 0.80, 0.86, 0.90, 0.95, 0.98 },
                θ = new double[] { 0.08, 0.22, 0.35, 0.46, 0.55, 0.64, 0.71, 0.78, 0.83, 0.88, 0.93, 0.97, 1.00 },
                tm = new double[] { 750, 641, 545, 461, 387, 322, 265, 215, 171, 132, 98, 69, 42 },
                Tg = new double[] { 692, 590, 500, 422, 353, 292, 239, 192, 150, 114, 82, 55, 30 },
                DifferenceTemperatures = new double[] { 58, 51, 45, 40, 35, 31, 27, 24, 21, 18, 16, 14, 12 }
            };

            HEVM.IsAuth = false;
            HttpContext.Session.Clear(); /*GetString("login")*/


            return View(nameof(Index) ,HEVM);
        }

        // POST ------------------------------------------------------------------------------- 
        [HttpPost]
        public IActionResult Index([FromForm] HeatExchangeInputViewModel HEVM)
        {
            var VM = new HeatExchangeViewModel();
            try
            {
                VM.Input = HEVM;
                var input = ConvertViewModelToInput(HEVM);

                var output = new HeatExchangeLib.HeatExchangeLib().Main(input);

                var outputViewModel = ConvertOutputToViewModel(output);
                VM.Output = outputViewModel;
            }
            catch (Exception ex)
            {
                VM.Message = "Введены некорректные значения!";

                VM.Output = new HeatExchangeOutputViewModel();
                VM.Output.m = 0;
                VM.Output.Y0 = 0;
                VM.Output.Y = new double[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                VM.Output.IntermediateValue_1 = new double[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                VM.Output.IntermediateValue_2 = new double[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                VM.Output.ϑ = new double[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                VM.Output.θ = new double[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                VM.Output.tm = new double[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                VM.Output.Tg = new double[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                VM.Output.DifferenceTemperatures = new double[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            }

            var login = HttpContext.Session.GetString("login");
            if (!string.IsNullOrWhiteSpace(login))
            {
                VM.IsAuth = true;
                VM.Login = login;

                var user = _dbСontext.Users.FirstOrDefault(X => X.Login == login);

                _dbСontext.Add(new History { UserId = user.Id, H0 = HEVM.H0, tm0 = HEVM.tm0, Tg0 = HEVM.Tg0, Wg = HEVM.Wg,
                    Cg =  HEVM.Cg, Cm = HEVM.Cm, Gm = HEVM.Gm, D = HEVM.D, Av = HEVM.Av});
                _dbСontext.SaveChanges();

                if (user != null)
                    VM.History = _dbСontext.History.Where(X => X.UserId == user.Id).ToList();
            }

            return View(VM);
        }


        // Функции -------------------------------------------------------------------------------------
        private HeatExchangeInput ConvertViewModelToInput(HeatExchangeInputViewModel HEIVM)
        {
            var vm = new HeatExchangeInput();
            vm.H0 = HEIVM.H0;
            vm.tm0 = HEIVM.tm0;
            vm.Tg0 = HEIVM.Tg0;
            vm.Wg = HEIVM.Wg;
            vm.Cg = HEIVM.Cg;
            vm.Cm = HEIVM.Cm;
            vm.Gm = HEIVM.Gm;
            vm.D = HEIVM.D;
            vm.Av = HEIVM.Av;

            return vm;
        }

        private HeatExchangeOutputViewModel ConvertOutputToViewModel(HeatExchangeOutput HEOVM)
        {
            var vm = new HeatExchangeOutputViewModel();
            vm.m = Math.Round(HEOVM.m, 2);
            vm.Y0 = Math.Round(HEOVM.Y0, 2);
            vm.IntermediateValue = HEOVM.IntermediateValue;
            vm.Y = HEOVM.Y;
            vm.IntermediateValue_1 = HEOVM.IntermediateValue_1;
            vm.IntermediateValue_2 = HEOVM.IntermediateValue_2;
            vm.ϑ = HEOVM.ϑ;
            vm.θ = HEOVM.θ;
            vm.tm = HEOVM.tm;
            vm.Tg = HEOVM.Tg;
            vm.DifferenceTemperatures = HEOVM.DifferenceTemperatures;

            return vm;
        }
    }
}
