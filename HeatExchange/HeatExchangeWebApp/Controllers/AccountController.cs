﻿using HeatExchangeWebApp.Data;
using HeatExchangeWebApp.Models;
using Microsoft.AspNetCore.Mvc;

namespace HeatExchangeWebApp.Controllers
{
    public class AccountController : Controller
    {
        private readonly ApplicationDbContext _dbContext;
        public AccountController(ApplicationDbContext context)
        {
            _dbContext = context;
        }

        [HttpGet]
        public bool Login([FromQuery] string login, [FromQuery] string password)
        {
            var user = _dbContext.Users.FirstOrDefault(X => X.Login == login);

            if (user == null)
                return false;

            if (user.Password != password)
                return false;

            HttpContext.Session.SetString("login", login);
            return true;
        }

        [HttpGet]
        public bool NewUser([FromQuery] string new_login, [FromQuery] string new_password)
        {
            var user = _dbContext.Users;
            foreach (var elem in user)
            {
                if (elem.Login == new_login)
                    return false;
            }
            _dbContext.Add(new Users
            {
                Login = new_login,
                Password = new_password
            });
            _dbContext.SaveChanges();
            return true;
        }
    }
}
