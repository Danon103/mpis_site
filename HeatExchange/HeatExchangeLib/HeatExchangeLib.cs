﻿namespace HeatExchangeLib
{
    public class HeatExchangeInput
    {
        public double H0 { get; set; } // Высота слоя, м
        public double tm0 { get; set; } // Начальная температура материала, градусы
        public double Tg0 { get; set; } // Начальная температура газа, градусы
        public double Wg { get; set; } // Скорость газа на свободное сечение шахты, м/с
        public double Cg { get; set; } // Средняя теплоемкость газа, кДж/(м^3 * К)
        public double Cm { get; set; } // Средняя теплоемкость материала, кДж/(м^3 * К)
        public double Gm { get; set; } // Расход материалов, кг/с
        public double D { get; set; } // Диаметр аппарата, м
        public double Av { get; set; } // Объемный коэффициент теплоотдачи, Вт/(м^3 * К)
    }

    public class HeatExchangeOutput
    {
        // Расчетные значения
        public double m;
        public double Y0;
        public double IntermediateValue;

        //Расчетные табличные значения
        public double[] Y;
        public double[] IntermediateValue_1;
        public double[] IntermediateValue_2;
        public double[] ϑ;
        public double[] θ;
        public double[] tm;
        public double[] Tg;
        public double[] DifferenceTemperatures;
    }

    public class HeatExchangeLib
    {
        public HeatExchangeOutput Main(HeatExchangeInput input)
        {
            HeatExchangeOutput output = new HeatExchangeOutput();

            output.m = m(input.Gm, input.Cm, input.Wg, input.D, input.Cg);
            output.Y0 = Y0(input.Av, input.H0, input.Wg, input.Cg);
            output.IntermediateValue = IntermediateValue(output.m, output.Y0);

            output.Y = Y(input.Av, input.Wg, input.Cg);
            output.IntermediateValue_1 = IntermediateValue_1(output.m, output.Y);
            output.IntermediateValue_2 = IntermediateValue_2(output.m, output.Y);
            output.ϑ = ϑ(output.IntermediateValue_1, output.IntermediateValue);
            output.θ = θ(output.IntermediateValue_2, output.IntermediateValue);
            output.tm = tm(input.tm0, input.Tg0, output.ϑ);
            output.Tg = Tg(input.tm0, input.Tg0, output.θ);
            output.DifferenceTemperatures = DifferenceTemperatures(output.tm, output.Tg);

            return output;
        }
        private List<double> y = new List<double> ();

        // Функция деления на 0,5 метровые слои
        private void DivisionIntoLayers (double H0)
        {
            for (double i = 0; i <= H0; i+=0.5)
                y.Add(i);
        }


        // Отношение теплоемекостей потоков
        public double m (double Gm, double Cm, double Wg, double D, double Cg)
        {
            if (Wg < 0 || D < 0 || Cg < 0)
                throw new ArgumentException("Одно из значений меньше нуля!");
            double S = Math.PI * (D / 2) * (D / 2);
            return (Gm * Cm) / (Wg * S * Cg);
        }

        // Полная относительная высота слоя
        public double Y0 (double Av, double H0, double Wg, double Cg)
        {
            if (Wg < 0 || Cg < 0 || H0 < 0)
                throw new ArgumentException("Одно из значений меньше нуля!");

            DivisionIntoLayers(H0);

            return (Av * H0) / (Wg * Cg * 1000);
        }

        // Промежуточное значение
        public double IntermediateValue(double m, double Y0)
        {
            if (m < 0)
                throw new ArgumentException("Значение m меньше нуля!");
            return 1 - m * Math.Exp((m - 1) * Y0 / m);
        }


        //-------------Табличные рассчеты-------------


        // Относительная высота
        public double[] Y (double Av, double Wg, double Cg)
        {
            if (Wg < 0 || Cg < 0)
                throw new ArgumentException("Одно из значений меньше нуля!");

            double[] Y = new double[y.Count];
            for (int i = 0; i < y.Count(); i++)
                Y[i] = (Av * y[i]) / (Wg * Cg * 1000);

            return Y;
        }

        //Промежуточное значение
        public double[] IntermediateValue_1(double m, double[] Y)
        {
            if (m < 0)
                throw new ArgumentException("Значение m меньше нуля!");

            double[] intermediateValue_1 = new double[y.Count];
            for (int i = 0; i < y.Count(); i++)
                intermediateValue_1[i] = 1 - Math.Exp((m - 1) * Y[i] / m);

            return intermediateValue_1;
        }

        //Промежуточное значение
        public double[] IntermediateValue_2(double m, double[] Y)
        {
            if (m < 0)
                throw new ArgumentException("Значение m меньше нуля!");

            double[] intermediateValue_2 = new double[y.Count];
            for (int i = 0; i < y.Count(); i++)
                intermediateValue_2[i] = 1 - m * Math.Exp((m - 1) * Y[i] / m);

            return intermediateValue_2;
        }

        // Относительная температура окатышей
        public double[] ϑ (double[] intermediateValue_1, double intermediateValue)
        {
            if (intermediateValue == 0)
                throw new ArgumentException("Значение intermediateValue равно нулю!");

            double[] ϑ = new double[y.Count];
            for (int i = 0; i < y.Count(); i++)
                ϑ[i] = intermediateValue_1[i] / intermediateValue;

            return ϑ;
        }

        // Относительная температура газа
        public double[] θ (double[] intermediateValue_2, double intermediateValue)
        {
            if (intermediateValue == 0)
                throw new ArgumentException("Значение intermediateValue равно нулю!");

            double[] θ = new double[y.Count];
            for (int i = 0; i < y.Count(); i++)
                θ[i] = intermediateValue_2[i] / intermediateValue;

            return θ;
        }

        // Конечная температура окатышей при определенной высоте
        public double[] tm (double tm0, double Tg0, double[] ϑ)
        {
            double[] t = new double[y.Count];
            for (int i = 0; i < y.Count(); i++)
                t[i] = tm0 + (Tg0 - tm0) * ϑ[i];

            return t;
        }

        // Конечная температура газа  при определенной высоте
        public double[] Tg (double tm0, double Tg0, double[] θ)
        {
            double[] T = new double[y.Count];
            for (int i = 0; i < y.Count(); i++)
                T[i] = tm0 + (Tg0 - tm0) * θ[i];

            return T;
        }

        // Разность температур окатышей и газа
        public double[] DifferenceTemperatures (double[] tm, double[] Tg)
        {
            double[] DT = new double[y.Count];
            for (int i = 0; i < y.Count(); i++)
                DT[i] = tm[i] - Tg[i];

            return DT;
        }
    }
}